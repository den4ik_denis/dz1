package com.DZ1.Function;


import java.util.Scanner;

public class Function {

    private static final String[] units = new String[]{"один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять"};
    private static final String[] lowDozens = new String[]{"одинадцать", "двенадцать", "тринадцать", "четырнадцать", "пятьнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать"};
    private static final String[] dozens = new String[]{"десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"};
    private static final String[] hundreds = new String[]{"сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};

    public void getDays(Scanner scanner) {

        System.out.println(" Enter number day of the week");

        if (scanner.hasNextInt()) {

            int numDay = scanner.nextInt();

            switch (numDay) {
                case 1:
                    System.out.println("Monday");
                    break;
                case 2:
                    System.out.println("Tuesday");
                    break;
                case 3:
                    System.out.println("Wednesday");
                    break;
                case 4:
                    System.out.println("Thursday");
                    break;
                case 5:
                    System.out.println("Friday");
                    break;
                case 6:
                    System.out.println("Saturday");
                    break;
                case 7:
                    System.out.println("Sunday");
                    break;
                default:
                    System.out.println("Write only numbers 1-7");
            }
        } else {
            System.out.println("Only numbers!");
        }
    }

    public int square(int a) {
        return a * a;
    }

    public double betweenPoints(int[] point1, int[] point2) {
        double length = Math.sqrt(square(point1[0] - point2[0]) + square(point1[1] - point2[1]));
        return length;
    }

    public String convertNumToString(int a) {
        if (a > 0 && a < 999) {
            int numHundreds = a / 100;
            int numDozen = a / 10 % 10;
            int numUnits = a % 10;
            if (numHundreds > 0 && numDozen == 1 && numUnits == 0) {
                return hundreds[numHundreds - 1] + " " + dozens[numDozen - 1];

            } else if (numHundreds == 0 && numDozen == 1 && numUnits == 0) {
                return dozens[numDozen - 1];
            } else if (numHundreds == 0 && numDozen > 1 && numUnits > 0) {
                return dozens[numDozen - 1] + " " + units[numUnits - 1];
            } else if (numHundreds > 0 && numDozen > 1 && numUnits > 0) {
                return hundreds[numHundreds - 1] + " " + dozens[numDozen - 1] + " " + units[numUnits - 1];
            } else if (numHundreds == 0 && numDozen > 1 && numUnits == 0) {
                return dozens[numDozen - 1];
            } else if (numHundreds > 0 && numDozen != 1 && numUnits == 0) {
                return hundreds[numHundreds - 1] + " " + dozens[numDozen - 1];
            } else if (numHundreds == 0) {
                return lowDozens[numUnits - 1];
            } else {
                return hundreds[numHundreds - 1] + " " + lowDozens[numUnits - 1];
            }
        } else if (a == 0) {
            return "ноль";
        } else {
            return "Numbers only 0 - 999";
        }
    }

    public int convertStringToNumber(String str) {
        int number = 0;
        String[] numbers = str.trim().split(" ");
        if (numbers.length == 3) {
            for (int i = 0; i < hundreds.length; i++) {
                if (hundreds[i].equalsIgnoreCase(numbers[0])) {
                    number = (i + 1) * 100;
                }
            }
            for (int i = 0; i < dozens.length; i++) {
                if (dozens[i].equalsIgnoreCase(numbers[1])) {
                    number += (i + 1) * 10;
                }
            }
            for (int i = 0; i < units.length; i++) {
                if (units[i].equalsIgnoreCase(numbers[2])) {
                    number += i + 1;
                    return number;
                }
            }
        }
        if (numbers.length == 2) {
            for (int i = 0; i < dozens.length; i++) {
                if (dozens[i].equalsIgnoreCase(numbers[0])) {
                    number += (i + 1) * 10;
                }
            }
            for (int i = 0; i < hundreds.length; i++) {
                if (hundreds[i].equalsIgnoreCase(numbers[0])) {
                    number += (i + 1) * 100;
                }
            }
            for (int i = 0; i < units.length; i++) {
                if (units[i].equalsIgnoreCase(numbers[1])) {
                    number += i + 1;
                    return number;
                }
            }
            for (int i = 0; i < lowDozens.length; i++) {
                if (lowDozens[i].equalsIgnoreCase(numbers[1])) {
                    number += i + 11;
                    return number;
                }
            }

        }
        if (numbers.length == 1) {
            for (int i = 0; i < units.length; i++) {
                if (units[i].equalsIgnoreCase(numbers[0])) {
                    number += i + 1;
                    return number;
                }
            }
            for (int i = 0; i < lowDozens.length; i++) {
                if (lowDozens[i].equalsIgnoreCase(numbers[0])) {
                    number += i + 11;
                    return number;
                }
            }
            for (int i = 0; i < dozens.length; i++) {
                if (dozens[i].equalsIgnoreCase(numbers[0])) {
                    number += (i + 1) * 10;
                    return number;
                }
            }
        }
        return 0;
    }
}