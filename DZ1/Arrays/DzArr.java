package com.DZ1.Arrays;

import java.util.Arrays;

public class DzArr {


    public int minArray(int[] arr) {

        int min = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (min > arr[i]) {
                min = arr[i];
            }
        }
        return min;
    }

    public int maxArray(int[] arr) {
        int max = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        return max;
    }

    public int indexMin(int[] arr) {

        int indexOfMin = 0;

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < arr[indexOfMin]) {
                indexOfMin = i;
            }
        }
        return indexOfMin;
    }

    public int indexMax(int[] arr) {

        int indexOfMax = 0;

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > arr[indexOfMax]) {
                indexOfMax = i;
            }
        }
        return indexOfMax;
    }

    public int sumOfElementWithOddIndex(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            if (i % 2 != 0) {
                sum += arr[i];
            }
        }
        return sum;
    }

    public int[] reverse(int[] arr) {
        int reverseArr[] = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            reverseArr[i] = arr[arr.length - 1 - i];
        }
        return reverseArr;
    }

    public int countOddElements(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 != 0) {
                sum++;
            }
        }
        return sum;
    }

    public int[] reverseHalf(int[] arr) {
        int half = arr.length / 2;
        int centre = half + arr.length % 2;
        int buffer;
        for (int i = 0; i < half; i++) {
            buffer = arr[i];
            arr[i] = arr[centre + i];
            arr[centre + i] = buffer;
        }
        return arr;
    }

    public int[] bubbleSorting(int[] arr) {
        int[] sorted = Arrays.copyOf(arr, arr.length);
        boolean isSorted = false;
        int buffer;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < sorted.length - 1; i++) {
                if (sorted[i] > sorted[i + 1]) {
                    isSorted = false;
                    buffer = sorted[i];
                    sorted[i] = sorted[i + 1];
                    sorted[i + 1] = buffer;
                }
            }
        }
        return sorted;
    }

    public int[] selectSorting(int[] arr) {
        int[] sorted = Arrays.copyOf(arr, arr.length);
        int indexMin = 0;
        int min = 0;
        for (int i = 0; i < sorted.length; i++) {
            indexMin = i;
            min = sorted[i];
            for (int j = i + 1; j < sorted.length; j++) {
                if (sorted[j] < min) {
                    indexMin = j;
                    min = sorted[j];
                }
            }
            sorted[indexMin] = sorted[i];
            sorted[i] = min;
        }
        return sorted;
    }

    public int[] insertSorting(int[] arr) {
        int[] sorted = Arrays.copyOf(arr, arr.length);
        int current = 0;
        int j = 0;
        for (int i = 1; i < sorted.length; i++) {
            current = sorted[i];
            j = i - 1;
            while (j >= 0 && current < sorted[j]) {
                sorted[j + 1] = sorted[j];
                j--;
            }
            sorted[j + 1] = current;
        }
        return sorted;
    }
}

