package com.DZ1.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] ARRAY = {3, 2, 1, 72, 25, 15};
        DzArr dzArr = new DzArr();

        dzArr.minArray(ARRAY);
        dzArr.maxArray(ARRAY);
        dzArr.indexMin(ARRAY);
        dzArr.indexMax(ARRAY);
        dzArr.sumOfElementWithOddIndex(ARRAY);
        dzArr.reverse(ARRAY);
        dzArr.countOddElements(ARRAY);
        dzArr.reverseHalf(ARRAY);
        dzArr.bubbleSorting(ARRAY);
        dzArr.selectSorting(ARRAY);
        dzArr.insertSorting(ARRAY);
    }
}
