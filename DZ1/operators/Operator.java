package com.DZ1.operators;

public class Operator {

    public static int a ;
    public static int b;
    public static int c;


         public int actionNumbers(int a, int b) {

    if (a%2==0){
        return(a*b);
    }
    else {
        return (a+b);
    }
    }

        public String quarterPointCoordinates(int a , int b){

             if (a<0 && b<0){
                 return"Your point is at 3 quarters of coordinates";
             }
             else if (a<0 && b>0){
                 return"Your point is at 2 quarters of coordinates";
             }
             else if (a>0 && b>0){
                 return"Your point is at 1 quarters of coordinates";
             }
             else {
                 return "Your point is at 4 quarters of coordinates";
             }
        }
        public int sumOfPositiveNumbers(int a, int b, int c){
              if (a>0 && b>0 && c>0){
                  return a+b+c;
              }
              else if (a>0 && b>0 && c<0){
                  return a+b;
              }
              else if (a>0 && b<0 &&c>0){
                  return a+c;
              }
              else if (a<0 && b>0 && c>0){
                  return b+c;
              }
              else {
                  return 0;
              }
    }

    public int calculateExpression (int a , int b, int c){
        if ( a * b * c > ( a + b + c ) ) {
            return a * b * c + 3;
        } else {
            return a + b + c + 3;
        }

    }

    public String studentRating (int rank){
             if (rank>=0 && rank <=19 ){
                 return"Rating this student F";
             }
             else if (rank>=20 && rank<=39){
                 return"Rating this student E";
             }
             else if (rank>=40 && rank<=59){
                 return"Rating this student D";
             }
             else if (rank>=60 && rank<=74) {
                 return"Rating this student C";
             }
             else if (rank>=75 && rank<=89) {
                 return"Rating this student B";
             }
             else if (rank>=90 && rank<=100) {
                 return"Rating this student A";
             }
             else {
                 return"Incorrect data for rating";
             }
    }
}
