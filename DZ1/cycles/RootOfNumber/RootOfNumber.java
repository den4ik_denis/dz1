package com.DZ1.cycles.RootOfNumber;

public class RootOfNumber {

    public static int sqRootSeq(int num)
    {
        for (int i=1; ; i++ )
        {
            int q = i * i;
            if (num == q)
                return i;
            if (num < q)
                return i-1;
        }
    }
}
