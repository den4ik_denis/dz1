package com.DZ1.cycles.primeNumber;

import java.math.BigInteger;

public class PrimeNumber {

    public static int number(Integer integer) {
        if (integer < 2) {
            System.out.println("Your number is not prime! ");
            return -1;
        }
        // тест Рабина-Миллера
            BigInteger bigInteger = BigInteger.valueOf(integer);
            boolean probablePrime = bigInteger.isProbablePrime((int) Math.log(integer));
            System.out.println(probablePrime);
            return 0;
        }
    }
