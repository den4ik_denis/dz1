package com.DZ1.cycles.FactorialNumber;

public class FactorialNumber {
    static int factorial = 1;

    public static int setFactorial(int n) {
        if (n >= 0) {
            for (int i = 1; i <= n; i++) {
                factorial = i * factorial;
            }
            System.out.println(factorial);
        }else {
            System.out.println("Only naturals have factorial! ");
        }
        return factorial;
    }
}