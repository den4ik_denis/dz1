package com.DZ1.Function;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FunctionTest {
    Function cut = new Function();

    @Test
    void squareTest () {
        int actual = cut.square(3);
        int expected = 9;
        assertEquals(expected, actual);
    }

    @Test
    void betweenPointsTest() {
        Object actual = cut.betweenPoints(new int[] {9, 6}, new int[] {5, 3});
        Object expected = 5.0;
        assertEquals(expected, actual);
    }
    @Test
    void convertNumToStringTest(){
        String actual = cut.convertNumToString(444);
        String expected = "четыреста сорок четыре";
        assertEquals(expected,actual);
    }

    @Test
    void convertStringToNumberTest(){
        int actual = cut.convertStringToNumber("пятьдесят четыре");
        int expected = 54;
        assertEquals(expected, actual);
    }

}
