package com.DZ1.cycles.RootOfNumber;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RootOfNumberTest {

    @Test
    void sqRootSeq() {

        int actual = RootOfNumber.sqRootSeq(36);
        int expected = 6;
        assertEquals(actual,expected);
    }

}