package com.DZ1.cycles.FactorialNumber;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FactorialNumberTest {

    @Test
    void factorial() {
        int actual = FactorialNumber.setFactorial(5);
        int expected = 120;
        assertEquals(actual,expected);
    }

}