package com.DZ1.cycles.SumNumbers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SumNumberTest {

    @Test
    void main() {
        int actual = SumNumber.getSum();
        int expected = 4950;
        assertEquals(actual, expected);
    }
}