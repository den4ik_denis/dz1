package com.DZ1.operators;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class OperatorTest {
    Operator cut = new Operator();

    @Test
    void actionNumbersTest() {
        int actual = cut.actionNumbers(3, 5);
        int expected = 8;
        assertEquals(expected, actual);
    }

    @Test
    void actionNumbersTest2() {
        int actual = cut.actionNumbers(4, 5);
        int expected = 20;
        assertEquals(expected, actual);
    }

    @Test
    void quarterPointCoordinatesTest() {
        String actual = cut.quarterPointCoordinates(-3, -5);
        String expected = "Your point is at 3 quarters of coordinates";
        assertEquals(expected, actual);
    }

    @Test
    void quarterPointCoordinatesTest2() {
        String actual = cut.quarterPointCoordinates(-8, 2);
        String expected = "Your point is at 2 quarters of coordinates";
        assertEquals(expected, actual);
    }

    @Test
    void quarterPointCoordinatesTest3() {
        String actual = cut.quarterPointCoordinates(3, 9);
        String expected = "Your point is at 1 quarters of coordinates";
        assertEquals(expected, actual);
    }

    @Test
    void quarterPointCoordinatesTest4() {
        String actual = cut.quarterPointCoordinates(11, -15);
        String expected = "Your point is at 4 quarters of coordinates";
        assertEquals(expected, actual);
    }

    @Test
    void sumOfPositiveNumbersTest() {
        int actual = cut.sumOfPositiveNumbers(2, 4, 6);
        int expected = 12;
        assertEquals(expected, actual);
    }

    @Test
    void sumOfPositiveNumbersTest2() {
        int actual = cut.sumOfPositiveNumbers(2, 4, -2);
        int expected = 6;
        assertEquals(expected, actual);
    }

    @Test
    void sumOfPositiveNumbersTest3() {
        int actual = cut.sumOfPositiveNumbers(2, -4, 6);
        int expected = 8;
        assertEquals(expected, actual);
    }

    @Test
    void sumOfPositiveNumbersTest4() {
        int actual = cut.sumOfPositiveNumbers(-2, 4, 6);
        int expected = 10;
        assertEquals(expected, actual);
    }

    @Test
    void calculateExpressionTest() {
        int actual = cut.calculateExpression(1, 2, -5);
        int expected = 1;
        assertEquals(expected, actual);
    }

    @Test
    void calculateExpressionTest2() {
        int actual = cut.calculateExpression(2, 3, 4);
        int expected = 27;
        assertEquals(expected, actual);
    }

    @Test
    void calculateExpressionTest3() {
        int actual = cut.calculateExpression(-2, -2, -2);
        int expected = -3;
        assertEquals(expected, actual);
    }

    @Test
    void studentRatingTest() {
        String actual = cut.studentRating(77);
        String expected = "Rating this student B";
        assertEquals(expected, actual);
    }
}