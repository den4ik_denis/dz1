package com.DZ1.Arrays;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DzArrTest {
    DzArr cut = new DzArr();
    @Test
    void minArrayTest() {
        int actual = cut.minArray(new int[]{1,2,3,2,22,10,17,-5,0,4,8});
        int expected = -5;
        assertEquals(expected, actual);
    }

    @Test
    void maxArrayTest() {
        int actual = cut.maxArray(new int[]{1,2,3,2,22,10,17,-5,0,4,8});
        int expected = 22;
        assertEquals(expected, actual);
    }

    @Test
    void indexMinTest() {
        int actual = cut.indexMin(new int[]{1,2,3,2,22,10,17,-5,0,4,8});
        int expected = 7;
        assertEquals(expected, actual);
    }

    @Test
    void indexMaxTest() {
        int actual = cut.indexMax(new int[]{1,2,3,2,22,10,17,-5,0,4,8});
        int expected = 4;
        assertEquals(expected, actual);
    }

    @Test
    void sumOfElementWithOddIndexTest() {
        int actual = cut.sumOfElementWithOddIndex(new int[]{1,2,3,2,22,10,17,-5,0,4,8});
        int expected = 13;
        assertEquals(expected, actual);
    }

    @Test
    void reverseTest() {
        int[] actual = cut.reverse(new int[]{1,2,3,2,22,10,17,-5,0,4,8});
        int[] expected = new int[]{8,4,0,-5,17,10,22,2,3,2,1};
        assertArrayEquals(expected, actual);
    }

    @Test
    void countOddElementsTest() {
        int actual = cut.countOddElements(new int[]{1,2,3,2,22,10,17,-5,0,4,8});
        int expected = 4;
        assertEquals(expected, actual);
    }

    @Test
    void reverseHalfTest() {
        int[] actual = cut.reverseHalf(new int[]{1,2,3,2,22,10,17,-5,0,4,8});
        int[] expected = new int[]{17,-5,0,4,8,10,1,2,3,2,22};
        assertArrayEquals(expected, actual);
    }

    @Test
    void bubbleSortingTest() {
        int[] actual = cut.reverseHalf(new int[]{1,0,3,2,-2,10});
        int[] expected = new int[]{2,-2,10,1,0,3};
        assertArrayEquals(expected, actual);
    }


    @Test
    void selectSortingTest() {
        int[] actual = cut.bubbleSorting(new int[]{1,2,3,2,22,10,17,-5,0,4,8});
        int[] expected = new int[]{-5,0,1,2,2,3,4,8,10,17,22};
        assertArrayEquals(expected, actual);
    }

    @Test
    void insertSortingTest() {
        int[] actual = cut.insertSorting(new int[]{1,2,3,2,22,10,17,-5,0,4,8});
        int[] expected = new int[]{-5,0,1,2,2,3,4,8,10,17,22};
        assertArrayEquals(expected, actual);
    }
}